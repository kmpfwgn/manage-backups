#!/bin/bash

# Usage: /bin/bash backup.sh <KEEP> <UPLOAD_TYPE> (<LIST_OF_ROLES)
# Example: /bin/bash backup.sh 4 ftp nginx mysql

SCRIPT=`readlink -f $0`
BASEDIR=`dirname $SCRIPT`

cd $BASEDIR

KEEP=$1
UPLOAD_TYPE=$2
ROLES=${@:3}

B_DIR="/var/tmp/bkp"
R_DIR="./roles"
T_DIR="./roles-templates"
HOST=`hostname`
PGP_KEY="75E55F9B"
UPLOAD_DIR="upload"
uploadFile=$UPLOAD_DIR/$UPLOAD_TYPE".sh"

SUF=`date +%s`

if ! [[ -d $B_DIR ]]
then
	mkdir $B_DIR
fi

echo
echo "-------------------------------------------------"
echo "---------------- Perform backups ----------------"
echo "::: "$(date "+%Y-%m-%d %H:%M:%S")
echo "::: Roles to backup: "$ROLES

echo
echo "::: Check if upload software installed: "
echo "/bin/bash $uploadFile checkInstalled"
/bin/bash $uploadFile checkInstalled
if [[ $? = 0 ]]; then echo "::: Software installed."; else echo "::: Software is not installed! Exit with code 1"; exit 1; fi

# Make backups by roles
for roleName in $ROLES
do
	echo
	echo "::: Current backup role: "$roleName

	role=$R_DIR/$roleName
	role_type=`head -1 $role | awk '{print $2}'`

	echo "::: Role type: "$role_type
	echo
	echo "::: Searching role template..."

	# Choose template for role
	if [[ -n "$role_type" ]]
	then
		template_file=$T_DIR/$role_type".sh"

		echo "::: Role template file: "`basename $template_file`
		echo

		len=`expr $(cat $role | wc -l) - 1`

		echo "::: Execute role template:"

		while read line
		do
			echo "/bin/bash $template_file $B_DIR/$HOST $SUF $line"

			/bin/bash $template_file $B_DIR/$HOST-$roleName $SUF $line
		done < <(tail -n "$len" $role)
	else
		echo "::: There is no template for such a type: "$role_type
		echo "::: Execute role:"
		echo "/bin/bash $role $B_DIR/$HOST $SUF"

		/bin/bash $role $B_DIR/$HOST $SUF
	fi

done

# Perform pgp encryption" 

echo
echo "::: Perform gpg encryption:"

for file in $B_DIR/*
do
	echo "gpg -o $file.gpg --encrypt -r $PGP_KEY $file"
	gpg -o $file.gpg --encrypt -r $PGP_KEY $file
	rm $file
done

# Upload

echo
echo "::: Perform upload. Method: "$UPLOAD_TYPE". Keep: "$KEEP

echo "/bin/bash $uploadFile $B_DIR $HOST $KEEP"
/bin/bash $uploadFile $B_DIR $HOST $KEEP
echo

echo "::: Remove temp directory..."
rm -rf $B_DIR
echo "::: Done!"
