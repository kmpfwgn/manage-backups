#!/bin/bash

M_USER="bkp.938471@gmail.com"
M_PASS="H0sEYjQhzy7oUfGH"
R_DIR="/Root/bkp"

if [[ "$1" = "checkInstalled" ]]
then
	if [[ $(dpkg -l megatools 2>/dev/null | tail -1 | awk '{print $2}') = "megatools" ]]; then exit 0; else exit 1; fi
fi

B_DIR=$1
HOST=$2
KEEP=$3

if [[ $(megals -u $M_USER -p $M_PASS --reload "$R_DIR/$HOST/" | wc -l) -eq 0 ]]
then 
	megamkdir -u $M_USER -p $M_PASS --reload "$R_DIR/$HOST"
fi

for file in $B_DIR/*
do
	fileShort=$(basename $file | sed 's/[0-9]*\..*//')

	echo
	echo "::: File to upload: "$fileShort
	echo "::: Need to rotate?"
	#rotate
	ls=`megals -u $M_USER -p $M_PASS "$R_DIR/$HOST/" | grep $fileShort"\."`
	if [[ $(echo "$ls" | wc -l ) -ge $KEEP ]]
	then
		echo "::: Yes."
		echo "::: Perform rotation:"
		rmFile=`echo "$ls" | head -1`
		echo "megarm -u $M_USER -p $M_PASS $rmFile"
		megarm -u $M_USER -p $M_PASS $rmFile
	else
		echo "::: No."
	fi

	echo "::: Perform upload:"
	megaput -u $M_USER -p $M_PASS --path "$R_DIR/$HOST/" $file
done
