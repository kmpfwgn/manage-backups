#!/bin/bash

LOGIN="bkp938471"
PASS="H0sEYjQhzy7oUfGH"
WEBDAV_SERVER="https://webdav.yandex.com"
REMOTE_B_DIR="backup"

if [[ "$1" = "checkInstalled" ]]
then
	if [[ $(dpkg -l curl 2>/dev/null | tail -1 | awk '{print $2}') = "curl" ]]; then exit 0; else exit 1; fi
fi

B_DIR=$1
HOST=$2
KEEP=$3

R_DIR=$REMOTE_B_DIR/$HOST

if [[ -z $(curl -s -u $LOGIN:$PASS $WEBDAV_SERVER -X PROPFIND -H "Depth: 1" | sed "s:href>:\n:g" | sed -n "s:^/\(..*\)<.*:\1:p" | grep $REMOTE_B_DIR) ]]
then
	curl -s -u $LOGIN:$PASS $WEBDAV_SERVER/$REMOTE_B_DIR -X MKCOL
fi
if [[ -z $(curl -s -u $LOGIN:$PASS $WEBDAV_SERVER/$REMOTE_B_DIR -X PROPFIND -H "Depth: 1" | sed "s:href>:\n:g" | sed -n "s:^/\(..*\)<.*:\1:p" | grep $HOST) ]]
then
	curl -s -u $LOGIN:$PASS $WEBDAV_SERVER/$REMOTE_B_DIR/$HOST -X MKCOL
fi

for file in $B_DIR/*
do
	fileBase=`basename $file`
	fileShort=$(echo $fileBase | sed 's/[0-9]*\..*//')
	echo
	echo "::: File to upload: "$fileBase
	echo "::: Need to rotate?"
	#rotate
	ls=`curl -s -u $LOGIN:$PASS $WEBDAV_SERVER/$R_DIR -X PROPFIND -H "Depth: 1" | sed "s:href>:\n:g" | sed -n "s:^/\(..*\)<.*:\1:p" | grep $fileShort"\." | grep -v "OLD"`
	if [[ $(echo "$ls" | wc -l ) -ge $KEEP ]]
	then
		echo "::: Yes."
		echo "::: Perform rotation:"
		rmFile=`curl -s -u $LOGIN:$PASS $WEBDAV_SERVER/$R_DIR -X PROPFIND -H "Depth: 1" | sed "s:href>:\n:g" | sed -n "s:^/\(..*\)<.*:\1:p" | grep $fileShort"\." | grep -v "OLD" | head -1`
		echo "curl -s -u $LOGIN:$PASS $WEBDAV_SERVER/$rmFile -X MOVE --header 'Destination: /$R_DIR/$fileShort.OLD' --header 'Overwrite: T'"
		curl -s -u $LOGIN:$PASS $WEBDAV_SERVER/$rmFile -X MOVE --header "Destination: /$R_DIR/$fileShort.OLD" --header "Overwrite: T"
	else
		echo "::: No."
	fi

	echo "::: Perform upload:"
	echo "curl -s -u $LOGIN:$PASS -T $file $WEBDAV_SERVER/$R_DIR/$fileBase"
	if [[ -z $(curl -s -u $LOGIN:$PASS -T $file $WEBDAV_SERVER/$R_DIR/$fileBase) ]]
	then
		echo "::: Upload success"
	else
		echo
		echo "::: Upload failed"
	fi
done
