#!/bin/bash

FTP_USER="bob"
FTP_PASS="122333"
FTP_SERVER="localhost"
FTP_PORT="2121"
R_DIR="backup"

if [[ "$1" = "checkInstalled" ]]
then
	if [[ $(dpkg -l lftp 2>/dev/null | tail -1 | awk '{print $2}') = "lftp" ]]; then exit 0; else exit 1; fi
fi

B_DIR=$1
HOST=$2
KEEP=$3

if [[ -z $(lftp -c "open -u $FTP_USER,$FTP_PASS $FTP_SERVER -p $FTP_PORT; ls $R_DIR" | grep $HOST) ]]
then
	lftp -c "open -u $FTP_USER,$FTP_PASS $FTP_SERVER -p $FTP_PORT; mkdir -p $R_DIR/$HOST"
fi

for file in $B_DIR/*
do
	fileShort=$(basename $file | sed 's/[0-9]*\..*//')

	echo
	echo "::: File to upload: "$fileShort
	echo "::: Need to rotate?"
	#rotate
	ls=`lftp -c "open -u $FTP_USER,$FTP_PASS $FTP_SERVER -p $FTP_PORT; ls -1 $R_DIR/$HOST" | grep $fileShort"\."`
	if [[ $(echo "$ls" | wc -l ) -ge $KEEP ]]
	then
		echo "::: Yes."
		echo "::: Perform rotation:"
		rmFile=`echo "$ls" | head -1`
		echo "lftp -c 'open -u $FTP_USER,$FTP_PASS $FTP_SERVER -p $FTP_PORT; rm -f $R_DIR/$HOST/$rmFile'"
		lftp -c "open -u $FTP_USER,$FTP_PASS $FTP_SERVER -p $FTP_PORT; rm -f $R_DIR/$HOST/$rmFile"
	else
		echo "::: No."
	fi

	echo "::: Perform upload:"
	echo "lftp -c 'open -u $FTP_USER,$FTP_PASS $FTP_SERVER -p $FTP_PORT; put -O $R_DIR/$HOST $file'"
	lftp -c "open -u $FTP_USER,$FTP_PASS $FTP_SERVER -p $FTP_PORT; put -O $R_DIR/$HOST $file"
done
