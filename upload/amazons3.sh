#!/bin/bash

s3Bucket="bkp-testbucket"
AWS="$HOME/.local/bin/aws"

if [[ "$1" = "checkInstalled" ]]
then
	if [[ (-n $(dpkg -l | grep pip)) && (-n $(pip show awscli)) && (-f $AWS) ]]; then exit 0; else exit 1; fi
fi

B_DIR=$1
HOST=$2
KEEP=$3

echo
echo "::: Bucket to upload: "$s3Bucket

for file in $B_DIR/*
do
	fileShort=$(echo `basename $file` | sed 's/[0-9]*\..*//')

	echo
	echo "::: File to upload: "$fileShort
	echo "::: Need to rotate?"
	#rotate
	ls=`$AWS s3 ls  "$s3Bucket/$HOST/" | awk '{print $4}' | grep $fileShort"\."`
	if [[ $(echo "$ls" | wc -l ) -ge $KEEP ]]
	then
		echo "::: Yes."
		echo "::: Perform rotation:"
		rmFile=`echo "$ls" | head -1`
		echo "$AWS s3 rm s3://$s3Bucket/$HOST/$rmFile"
		$AWS s3 rm "s3://$s3Bucket/$HOST/$rmFile"
	else
		echo "::: No."
	fi

	relativePath="$s3Bucket/$HOST/$fileName"
	echo "::: Perform upload:"
	$AWS s3 cp $file "s3://$relativePath"
done
