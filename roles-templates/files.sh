#!/bin/bash

PREF=$1
SUF=$2
ARG=${@:3}

PARAMS=`echo $ARG | awk '{$1=""; print $0}'`
B_PATH=`echo $ARG | awk '{print $1}'`
NAME=`echo $B_PATH | sed 's:/:-:g'`

cd $B_PATH
echo "tar -czf $PREF$NAME.$SUF.tar.gz "./" $PARAMS"
tar -czf $PREF$NAME.$SUF.tar.gz "./" $PARAMS

exit 0
