#!/bin/bash

PREF=$1
SUF=$2
ARG=${@:3}

dCommand=`echo $ARG | cut -d "|" -f1`
dBases=`echo $ARG | cut -d "|" -f2`

for base in $dBases
do
	S_FILE=$PREF-db-$base.$SUF.sql
	echo "$dCommand $base > $S_FILE"
	$dCommand $base > $S_FILE
	echo 'gzip -9 '"$S_FILE"' -S ".gz"'
	gzip -9 $S_FILE -S ".gz"
done

exit 0
